import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard ({ product }) {

  const { _id, name, description, price } = product;

  return (

      <Card className="cardHighlight col-lg-4 col-md-6">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
          <Link className="btn btn-primary" to={`/products/${_id}`} >Details</Link>
        </Card.Body>
      </Card>

  )
}
