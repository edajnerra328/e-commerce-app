import { useContext } from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import UserContext from "../UserContext";

export default function AppNavBar () {

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="primary" expand="lg">
      <Container>
        <Navbar.Brand as={ Link } to="/" className="text-white">RenJad</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={ NavLink } to="/" className="text-white">Home</Nav.Link>
            <Nav.Link as={ NavLink } to="/products" className="text-white">Our Store</Nav.Link>
            {
              (user.id !== null) ?
                <Nav.Link as={ NavLink } to="/logout" className="text-white">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={ NavLink } to="/login" className="text-white">Login</Nav.Link>
                  <Nav.Link as={ NavLink } to="/register" className="text-white">Register</Nav.Link>
                </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}