// Bootstrap Grid System Components (row, col)
import { Row, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function Banner ({isValidRoute}) {
  return (
    <Row className='bg-secondary mt-3 text-white'>
      {isValidRoute ? (
        <Col className="p-5">
          <h1 className='text-center'>Welcome To RenJad Store</h1>
          <p className='text-center'>Shop All You Want</p>
        </Col>
      ) : (
        <Col className="p-5">
          <h1 className='text-center'>Page Not Found</h1>
          <p className='text-center'>Go back to the <Link as={ Link } to="/">homepage.</Link></p>
        </Col>
      )}
    </Row>   
  )
}