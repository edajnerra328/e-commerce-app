import { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register () {

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // Function to simulate user registration
  function registerUser(e) {

  // Prevents page redirection via form submission
  e.preventDefault();

  // Make GET request to check if email already exists
  fetch(`https://capstone2-matunog.onrender.com/api/users/checkEmail`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data) {
        // Email already exists, show error message
        Swal.fire({
          title: "Duplicate email found",
          icon: "error",
          text: "Please provide a different email.",
        });

        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
        setPassword1(password1);
        setPassword2(password2);
      } else {
        // Email does not exist, make POST request to register user
        fetch(`https://capstone2-matunog.onrender.com/api/users/register`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password1,
          }),
        })
          .then((res) => res.json())
          .then((data) => {

            if (typeof data !== "undefined") {

              Swal.fire({
                title: "Registration Successful",
                icon: "success",
                text: "Welcome to Zuitt!",
              });

              navigate("/login");

              // Reset the state values inside the `else` block
              setFirstName("");
              setLastName("");
              setEmail("");
              setPassword1("");
              setPassword2("");
            } else {
              Swal.fire({
                title: "Error",
                icon: "error",
                text: "Registration failed. Please try again.",
              });
            }
          });
      }
    });
}

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password1.length >= 8 && password2 !== "" && password1 === password2) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);

  return (

    <Row className="register justify-content-center align-items-center">
      <Col className="d-flex justify-content-center align-items-center">
        <Card className="px-md-4">
          <Card.Body>
            <Form onSubmit={(e) => registerUser(e)}>
              <h1 className="text-center">Register</h1>
              <Form.Group className="mb-3" controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Enter First Name" 
                  value={ firstName } 
                  onChange={e => setFirstName(e.target.value)}
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Enter Last Name" 
                  value={ lastName } 
                  onChange={e => setLastName(e.target.value)}
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                  type="email"
                  placeholder="Enter email" 
                  value={ email } 
                  onChange={e => setEmail(e.target.value)} 
                  required/>
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Password" 
                  value={ password1 } 
                  onChange={e => setPassword1(e.target.value)} 
                  required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Verify Password" 
                  value={ password2 } 
                  onChange={e => setPassword2(e.target.value)} 
                  required/>
              </Form.Group>

              {
                isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                  Submit
                </Button> : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
              }
            </Form>
          </Card.Body>
        </Card>
      </Col>
    </Row>

  )
};